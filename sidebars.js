module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Presentation',
      items: [
        {
          type: 'doc',
          id: 'presentation/intro', // document id
          label: 'Project overview', // sidebar label
        },
        'presentation/getting-started',
        'presentation/operational-implementation',
        {
          type: 'category',
          label: 'Features',
          items: [
            'presentation/features/webapp',
            'presentation/features/zones',
            'presentation/features/wifi',
            'presentation/features/wired',
            'presentation/features/ota-updates',
            'presentation/features/qos',
            'presentation/features/multi-tenancy',
            'presentation/features/vpn',
            'presentation/features/secops',
          ],
        },
        'presentation/limitations',
      ],
    },
    {
      type: 'category',
      label: 'Cloud controller',
      items: [
        {
          type: 'doc',
          id: 'controller/intro', // document id
          label: 'Overview', // sidebar label
        },
        "controller/requirements",
        {
          type: 'doc',
          id: 'controller/preparation', // document id
          label: 'Preparation', // sidebar label
        },
        "controller/dns",
        {
          type: 'doc',
          id: 'controller/installation', // document id
          label: 'Installation', // sidebar label
        },
        "controller/initial-login",
        {
          type: 'doc',
          id: 'controller/config-reference', // document id
          label: 'Config file reference', // sidebar label
        },
        {
          type: 'doc',
          id: 'controller/maintenance-mode', // document id
          label: 'Maintenance Mode', // sidebar label
        },
      ],
    },
    {
      type: 'category',
      label: 'Hardware',
      items: [
        'hardware/supported-hardware'
      ],
    },
    {
      type: 'category',
      label: 'Technical guide',
      items: [
        'technical-guides/installation-os',
        'technical-guides/local-web-status-page',
        'technical-guides/ssh-connection',
        'technical-guides/persistent-logs',
        'technical-guides/serial-port-connexion',
        'technical-guides/wan-configuration',
        'technical-guides/qos',
        'technical-guides/factory-reset',
        'technical-guides/appliance-status',
        'technical-guides/api',
        'technical-guides/service-library'
      ],
    },
    {
      type: 'category',
      label: 'User guides',
      items: [
        {
          type: 'category',
          label: 'Dashboard',
          items: [
            'user-guides/dashboard/getting-started',
            'user-guides/dashboard/sentinelc-router-initial-onboarding',
            'user-guides/dashboard/dashboard-overview',
            'user-guides/dashboard/installation-access-point',
            'user-guides/dashboard/replace-failing-appliance',
            'user-guides/dashboard/password-account',
            'user-guides/dashboard/firmware-update',
            'user-guides/dashboard/ports-status',
            'user-guides/dashboard/blocking-device-network',
            'user-guides/dashboard/add-user',
            'user-guides/dashboard/add-location',
            'user-guides/dashboard/add-wi-fi',
            'user-guides/dashboard/extend-wi-fi',
            'user-guides/dashboard/wi-fi-schedule',
            'user-guides/dashboard/create-zone',
            'user-guides/dashboard/internet-with-static-ip'
          ],
        },
        {
          type: 'category',
          label: 'Admin portal',
          items: [
            'user-guides/admin-portal/access',
            'user-guides/admin-portal/manage-accounts',
            'user-guides/admin-portal/manage-administrators',
            'user-guides/admin-portal/manage-locations',
            {
              type: "category",
              label: "Appliances",
              items: [
                'user-guides/admin-portal/manage-appliances/listing-appliances',
                'user-guides/admin-portal/manage-appliances/appliance-details',
                'user-guides/admin-portal/manage-appliances/inventory',
                {
                  type: 'doc',
                  id: 'user-guides/admin-portal/manage-appliances/operations',
                  label: 'Operations', // sidebar label
                },
                'user-guides/admin-portal/manage-appliances/disks',
                'user-guides/admin-portal/manage-appliances/embedded-services',
              ]
            },
            'user-guides/admin-portal/manage-devices',
            'user-guides/admin-portal/manage-connections',
            'user-guides/admin-portal/manage-incidents',
            'user-guides/admin-portal/manage-projects',
            'user-guides/admin-portal/packet-captures',
            'user-guides/admin-portal/wi-fi-scans',
            'user-guides/admin-portal/port-scans',
            'user-guides/admin-portal/manage-tasks',
            'user-guides/admin-portal/manage-administrators',
            'user-guides/admin-portal/manage-service-library'
          ],
        },
      ],
    },
    {
      type: 'category',
      label: 'FAQ',
      items: [
        'faq/installation',
        'faq/wi-fi-network-internet',
        'faq/update-firmware',
        'faq/web-mobile-application',
        'faq/connection-port',
        'faq/manage-and-administration',
        'faq/administrative-support'
      ],
    },
    {
      type: 'category',
      label: 'Release notes',
      items: [
        'release-notes/release-2022-01-31-p28'
      ],
    },
  ],
};
