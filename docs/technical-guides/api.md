---
title: API
---

## Management commands

Management commands are used to perform specific actions in the API.


### Usage
To run a management command, you must first log in into the controller and go in the `apps` directory. For example:

```bash
cd my-sentinelc/apps
```

After that, you can run the desired command with:

```bash
docker-compose exec api ./manage.py command
```

Replace `command` with one of the following available commands.

### Commands

#### create_site

This command is to create a new site in the application. Sites are used as endpoints for the appliances.

##### Options

&emsp; *--domain [required]*: The domain name for the new site

&emsp; *--name [required]*: The display name for the new site

##### Usage

```bash
./manage.py create_site --domain DOMAIN --name NAME
```
