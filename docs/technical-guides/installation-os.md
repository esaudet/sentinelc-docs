---
title: Installation OS
---

## Architecture ARM RPI4

### Download image

Download Yocto image for ARM based on Yocto linux and that complete, pre-compiled images are available to flash 

And unzip the downloaded zip file to get a .biosimg.bz2 file.

### Flash image

Then, you just have to flash this image on the SD card or SSD you want to use with you Device based on ARM, for example Raspberry Pi 4.

The software recommended is Etcher (Linux/MacOS/Windows compatible).

Install Etcher, plug your SD card or SSD into your computer, and flash the .biosimg.bz2 file on your SD card.

![Etcher](installation-os/intel-etcher.PNG)

### Plug your device

Plug your RPI4 to your local network and boot.


## Architecture Intel/AMD

### Download image

Download Yocto image for Intel-AMD based on Yocto linux and that complete, pre-compiled images are available to flash 

And unzip the downloaded zip file to get a .biosimg.bz2 file.

### Flash image

Then, you just have to flash this image on the SD card or SSD you want to use with you Device based on Intel-AMD.

The software recommended is Etcher (Linux/MacOS/Windows compatible).

Install Etcher, plug your SD card or SSD into your computer, and flash the .biosimg.bz2 file on your SD card.

![Etcher](installation-os/intel-etcher.PNG)

### Plug your device

Plug your device to your local network and boot.

## Configure on Virtual machine

OS configuration in VirtualBox.

Tested on VirtualBox version 6.1.22 (Windows).

### Step 1: Download image

http://www.sentinelc.com/OS

The file will have the extension .biosimg.bz2 

### Step 2: Converting the image from a raw format to VDI 

Unzip the raw image: 

```shell
bunzip2 drone-xxx.biosimg.bz2
```

The "VBoxManage" utility provided by VirtualBox allows you to convert from a raw format to VDI with the following command: 

```shell
VBoxManage convertfromraw input_file.biosimg output_file.vdi --format VDI 
```

> Sources: https://www.virtualbox.org/manual/ch08.html#vboxmanage-convertfromraw

### Step 3: Adding the image in VirtualBox 

Once the image is converted, add it in VirtualBox with : 

`*File -> Virtual Media Manager -> Add -> Choisir le fichier précédemment converti en .VDI.`

### Step 4: Configuration to connect another VM via an internal network 

The VM configuration page is exposed by a local server and can be accessed with a browser.
To access it, we can configure another VM linked to the first by an "internal network". 

Create a second generic Linux VM in VirtualBox.
In the "Network" options of this new VM, configure adapter 1 like this: 


** Adapter 1 **
```shell
Attached to : Internal Network
Name: name-of-network
Advanced/Promiscuous mode : Allow VMs
```

** Adapter 1 **
```shell
Attached to : Bridged Adapter
Advanced/Promiscuous Mode : Allow VMs
```

** Adapter 2 **
```shell
Attached to : Internal Network
Name: name-of-network
```

The configuration page is accessible by the second VM at the default address 192.168.99.1:9888. 

### Step 5: VM configuration 

The VM is still devoid of any configuration. The page 192.168.99.1:9888 allows you to remedy this.
A blue message indicates to specify the API server.

Enter https://api.dev.sentinelc.com.

The device will issue a registration request, which must be accepted by an administrator.

Once the device has been registered, modify the device from the management portal:
Port 0 must be in WAN mode, and port 1 in ACCESS mode with a "private" VLAN. 