---
title: SSH Connection

---

## Compatible device with SentinelC Solution:

- ARM RPI4 architecture 
    - RPI4
- Intel/AMD architecture
    - APU2
    - Lanner
    - Qotom
- Virtual Machine


## Connection to SentinelC router by SSH

Make sure you have activated the VPN before connecting.

- Add your SSH key in Django
- Select the Router in Django and add its key in Authorized keys
- Connection to the router: Start the terminal: `ssh root@ip from the router`

If you have access via a jumpbox, connect to the jumpbox first with `ssh -A`.

### Diagnosing with systemd

Basic order:

- Service status: `systemctl status`
- Start a service: `systemctl start nom_du_service.service`
- Stop a service: `systemctl stop nom_du_service.service`
- restart a service: `systemctl restart nom_du_service.service`

### SentinelC command

A cli command `sentinelc` offers a few actions:

```
# sentinelc --help
usage: sentinelc [-h] {fetchconfig, clean-up, reset, dhcp-probe} ...

positional arguments:
  {fetchconfig, clean-up, reset, dhcp-probe}
                        commands
    fetchconfig Fetch and show device configuration
    clean-up Clean-up local files and reboot.
    reset Local factory reset and reboot.
    dhcp-probe Perform a DHCP probe on all vlans.

optional arguments:
  -h, --help show this help message and exit
```

No order is risky. Even the factory reset. The device will simply reboot and find its same configuration via the API. A device does not have the necessary permissions to modify its own configuration in the API.

The only thing that can be lost is the local WAN configuration. See [WAN-configuration](WAN-configuration).

### Logs

All logs are available via `journalctl`.

By default the logs are in RAM only. See [Persistent logs](Persistent-logs).

### Configuring the SentinelC agent

The SentinelC agent (chinookd) stores its configuration on the persistent partition in `/ data / chinook /`.

Most of the files are there to allow an operation even if there is an internet failure or API failure.

The only important file that cannot be recreated is the manual configuration of the internet connection. See [WAN-configuration](WAN-configuration).

