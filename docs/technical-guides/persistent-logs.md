---
title: Persistent logs
---

## Enable persistent logs

`systemd-journald` is responsible for logs in the OS.

By default, the logs are kept only in RAM.

For the logs to survive reboots and be available for analysis:

```
rm /var/log
cp -a /var/volatile/log/ /var/
mkdir /var/log/ journal
reboot
```

Subsequently, the `journalctl` command can be used to read the logs stored on the disk.

## To cancel

We can do a factory reset.

Or:

```
rm -rf /var log
ln -sf /var/volatile/log/var
reboot
```