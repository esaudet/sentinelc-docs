---
title: Factory reset
---

## Quick explanation

- You can do a factory reset in the Django admin or directly on the device.
- Via the admin, the cloud configuration and the device configuration is reset.
- Via the device, only the device data is reset.
  - After a reboot, the device re-connects to the API and will therefore resume the same configurations.
  - This can be useful only for resetting a device during testing or debugging.

Local reset:

```
sentinelc reset
```




## Full explanation

The factory reset is an operation that can be done via the django admin.

It mainly affects 2 data:

- Device configuration and assignment data in the central API.
- Complete reset of the persistent data in the device.

The first part is done in the API data and works for a long time.

For the second part, a firmware 0.33.0 or higher is required for the complete cleanup to be performed. Before this version, the configuration files were re-generated but some data and cache could remain on the device.


### Central API

In the central database:

- Device display name (device location) is reset to null.
- The device is removed from its network location.
  - If it was in "Router" mode, the network location is invalid (no router).
  - In Access Point mode, the network location will have one less access point but still valid.
  - If the device was the only one broadcasting an SSID, this SSID will be unreachable because no terminal is broadcasting it.
  - SSIDs broadcast by the device are removed.
- If the device was initialized by onboarding, the onboarding data is removed.
  - This makes the device available for new onboarding.
- The operation mode is reset to ROUTER.
- The Release Channel (tag) is set to STABLE.
  - This may trigger a firmware deployment if necessary. See operation scheduling below.
- SSH keys allowed to connect to the device via the VPN are removed.
- The Poll Interval is reset to 60 seconds.
- A new random `initialization_id` is generated. This is what will trigger the persistent data cleanup (re-initialization).


### On the device

The command to perform a factory reset is done via the same mechanism as all other configuration changes.

The device receives its configuration at every startup, every 60 seconds (poll interval) or via a PING from the device via the webhook system.

The procedure works even if the device is offline when the factory reset is performed.

In the case of a factory reset, the `initilization_id` field will have changed. The device will detect this change and trigger the reset process.

We only keep:

- `/etc/hostname`: Hostname of the device, example sentinelc-aaa111
- `/data/chinook/device_id`: Device ID of the device, example: `aaa111`.
- `/data/chinook/api_token`: The token that authorizes the device to talk to the API
- `/data/chinook/api_endpoint`: URL of the api (allows to point to the DEV or PROD).
- `/data/mender/device_type`: Configuration for firmware updates
- `data/mender_grubenv.config`: Configuration for firmware updates

All other files are deleted.

### Scheduling of operations

- A technician performs the Reset via the admin.
- The data from the central PLC is immediately affected.
- A new `initilization_id` is generated and made available to the device as a configuration key.
- The status of the device will be set to [DIRTY] because a configuration change is pending.
- The device, in its next polling, detects the change of `initilization_id`.
- All non-essential files are deleted.
- REBOOT.
- The configuration of the network cards is re-generated. This puts the device back into router mode.
- REBOOT.
- Re-generation of SSH host keys.
- Re-generation of VPN keys.
- Confirmation to the API that the new configuration is applied. The [DIRTY] status is removed, the status changes to GREEN.
- If the reset causes a firmware update to be triggered (back to STABLE):
  - Firmware download
  - REBOOT
  - Confirmation


### Rotations of secrets

#### The process causes a rotation of the following secrets:

- SSH Host keys
  - There will be a warning the next time you connect to the device because the SSH client will detect that the host keys have changed.
  - We can validate that we are connecting to the right device because the new SSH public keys are visible in the django admin.
- VPN wireguard keys
  - The old keys are deleted and the device will generate new VPN keys for itself.
  - The public key is sent to the Central API.
  - The VPN server will (eventually) put the new key into operation and remove the old one.
  - This can take up to 5 minutes.
- The `radius_secret` of the device is re-generated to a new random value.

#### Secrets that are not changed at this time:

- The SentinelC registration key.
  - This key is printed on the device and would be inconvenient to change.
- The API authentication token.
  - This is the password that allows the device to authenticate to the API.
  - It would be preferable to be able to rotate this token.

