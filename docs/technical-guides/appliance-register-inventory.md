---
title: Appliance register to inventory
---


## How to register to inventory

- One or more supported [devices](../hardware/supported-hardware) that you will flash with the SentinelC OS.

- Flash SentinelC OS on your appliance

- Add IP public adress on admin portal in manage inventory page. 

To know the public IP address of your appliance, go to https://ifconfig.me/

![Manage inventory](./manage-inventory.png)

Enter this IP in Inventory settings.

![Add IP](./add-ip.png)

- Once the new appliance is detected in the admin portal, add it to the inventory. To do this, select the appliance and use the **add to inventory** action.

![Add inventory](./add-inventory-appliance.png)

Enter this location and **Confirm** for add appliance in inventory.

![Add inventory confirm](./add-inventory-appliance-confirm.png)

Your appliance is now in the inventory, so it is possible to install it as a router in the SentinelC application, or to claim it for an organization.

- To install it, go to app.sentinelc.example.org and use the registration key to do the installation.

- To claim it, select your appliance in stock and use the **Transfer to** action.

![Transfert to](./transfert-to.png)

Select the location to claim the appliance and confirm.

![Transfert to](./transfert-to-confirm.png)