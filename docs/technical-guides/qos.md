---
title: QoS
---

## Configuration

### 1. Create the WAN connection profile

In the network location, create a WAN connection profile if there is not one already.

Enter values to limit the total upload and download available.

![wan-profil](./wan-profil.PNG)

In the above example, we have 2 connection profiles: The first one with QoS enabled, the second one without QoS. The QoS is activated as soon as the speed is defined.

No additional calculation is done automatically on the speed. So we have to apply the 90% rule on the entered value ourselves.

The value is expressed in megabits per second. Decimals can be entered.

The rule of thumb is to put about 90% of the speed of the connection. See explanations above.



### 2. Activate the QoS profile

On the main router, activate our WAN connection profile on the WAN port:

![activate-qos](./activate-qos.PNG)

That's it!

Each zone will have an equal percentage of guaranteed bandwidth and each device in each zone will also have an equal share.


### 3. Limit an area

It is also possible to limit or guarantee an amount of bandwidth on a particular area:

![zone-limited](./zone-limited.PNG)


In this example, the maximum total download for the area will be 2mbps and the upload will be 0.5mbps.

In case of shortage, this limit could be even smaller. It is not guaranteed.

Even if there is no other activity on the network, the zone will never exceed this speed.


## QoS Status   

In order to verify that the Qality of Service is activated, you just have to check the web status of the SentinelC appliance, if the QoS is active, a graph appears with Ingress and Egress.

View [Local web status page](./local-web-status-page) for view connection. 

![qos-status](./qos-status.PNG)

