---
title: Service library
---


## What is it?


The service library is a set of apps that are maintained by the SentinelC team.


![demo library example](service-library/sentinelc-app-library.jpg)


This library is actually [a git repository](https://gitlab.sentinelc.com/sentinel-c/app-library-demo)* containing multiple manifest files for all the apps shown.


The manifest file contains all the information needed to launch the service:
 - The actual launch script
 - The parameters with some validations
 - The information to expose the app to the network zone or the cloud


## How does it work ? 
![demo library example](service-library/sentinelc-service-diagram.png)


- An admin registers a repository inside the SentinelC API.
- The API fetch service definitions from the appfeed and imports it
- A user uses the service definitions to install an app to an appliance
- The app installation script is sent to the appliance and installed using podman (container)
- The API creates a host for the app to treat it as a host
- The vpn server integrate that host inside the vpn and adds a proxy entry to access the app
- The app can be accessed with the cloud proxy, the local ports or a web-based terminal


## How to use it ?


### As the SentinelC maintainer

To add services to your app library, you need to first add a service repository.
To do so, access the django admin page of your SentinelC instance and navigate to the "Repositories" tab under "Pods".
Select "Add repository" and fill the information with the name of your feed and the url of [the json feed (see library builder)](https://gitlab.sentinelc.com/sentinel-c/app-library-builder)*

![Django admin page](service-library/sentinelc-repository-django.jpg)

Once the repository is added, it will automatically update itself each hour to import the latest services.

### As a user
The services can be launched with a few clicks :
    - Click on the arrow of the service tab inside the appliance page
    - Click on "install a service"
    - Select the service to launch
    - Select the zone to launch it in
    - Click on the "install button"
    - Fill the parameters of the service
    - Click "OK"


Once all those steps are done, you should have a service installed on your appliance in the service tab that is ready to launch.
Simply launch the service in the service tab inside appliances and access it with the configured access (cloud/LAN/terminal).


## How do I add services to it ?


To add some apps you will need to use the [library builder tools](https://gitlab.sentinelc.com/sentinel-c/app-library-builder)*.


Use to link to get updated information on :
 - Manifest file specifications
 - Service creation/validation tools
 - Deployement

*Url will be updated when the repositories are made public