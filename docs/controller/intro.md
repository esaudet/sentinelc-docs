---
title: Cloud controller overview

# TODO: Insert link to api registration on line 22 (You will only need to register your devices..)
---

SentinelC devices are designed to be remotely controlled from a cloud-based API server.

The main features of the cloud controller are:

- Manage network administrators
- Manage device inventory and ownership
- Centralize all configurations
- Monitor managed devices
- Collect and analyze network usage
- Alert administrators of abnormal behaviors

By operating and owning your own cloud controller instance, you are fully in control of your configurations, data and personal information. The distributed software is free from 3rd party data collection and all data remains on your servers unless you move it yourself.

Please note that the cloud controller is a multi-tenant solution. An operator can deploy a single, central cloud controller installation to manage multiple distinct organizations and networks and delegate access.

If you wish to use a cloud controller operated by a 3rd party, you do not need to read this section. You will only need to register your devices with the API endpoint provided by your operator.

At this moment we do not provide any publicly available hosted solutions for the cloud controller.

## Main components

The following components will be installed by following this guide:

### API Server

The API server is a [Django](https://www.djangoproject.com/) based project. It remotely controls the managed SentinelC devices, and allows administrators to monitor and interact with the system through a GraphQL API.

It is available under the `api` subdomain.

### Front-end web application

The main GUI application. It is a static web application optimized for desktop and mobile browsers. It is built using the [React JavaScript](https://reactjs.org/) library.

It is available under the `app` subdomain.

### Keycloak

[Keycloak](https://www.keycloak.org/) is an open-source identity and access management solution. This is where the login credentials are stored and it handles the login, password reset and account creation features.

Keycloak is used as an off-the-shelf solution. The API Server interacts with its REST API to provision new user accounts.

It is available under the `accounts` subdomain.

### WiFi Portal

This is a very small static web page to serve captive WiFi portal to visitors.

It is available under the `portal` subdomain.

### Proxy service

The 4 components above are accessible over HTTPS. We provide a built-in HTTPS proxy based on an [nginx](https://www.nginx.com/) container. The provided configuration will route requests to the appropriate container and terminate SSL connections.


### VPN Server

An automated VPN is securely established between the cloud components and SentinelC devices.

The VPN is based on the [Wireguard](https://www.wireguard.com/) project.


### Log collection service

Managed devices are automatically configured to push their system logs and metrics to a log collection service. This solution powers the live logs and the metrics widgets on the appliance details page.

This specialized service is deployed under the `logs` subdomain.


## Distribution

The entire cloud controller solution described above is distributed as a set of docker containers.

A [docker-compose](https://docs.docker.com/compose/) script is provided to configure, link and run the containers easily.

While it is possible to run distributed, fault-tolerant instances on docker schedulers such a Kubernetes, official support and documentation is still in progress. You are welcome to experiment and to share your results.

Being docker-based, the controller is expected to run on a modern Linux host.
