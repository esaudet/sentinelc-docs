---
title: Requirements

---

## Hardware resources

System memory, CPU and disk space requirements will vary based on many factors. The bare minimum requirements are 2gb of ram and 25gb of disk space. As you add more managed devices and users, you will need to increase these specs.

## Networking

Your server or VM need to be accessible by your managed SentinelC devices as well as by yourself and any other administrators. While it is possible to install the server behind a firewall or on-prem, the most common approach is to run on a publicly accessible cloud server.

## Domain name

You will need a domain or a subdomain with DNS that you control.

The installation of a cloud controller requires a few sub-domains to be pointed the the public IP address of your server or VM. More details will be provided in the [DNS configuration](dns) section.

## Mail server

The cloud controller needs access to a mail server using the SMTP protocol to send outgoing mail. These emails include account activation, password resets and notifications.

## Software dependencies

- A fresh Ubuntu 20.04 linux VM
- Wireguard kernel module (included by default with Ubuntu 20.04)
- Wireguard tools
- Docker and docker-compose

The [Preparation](preparation) section will guide you through installing these dependencies.

:::caution

This guide expects the installation to be performed on a fresh linux server or virtual machine that will be dedicated to your SentinelC installation.

:::
