---
title: Cloud controller installation

---


:::tip

Make sure you have read the previous chapters, including [host preparation](preparation) and [dns configuration](dns) before continuing.

Approximate installation time : 30 min.

:::

## Clone the env-builder repository

The `env-builder` contains the `docker-compose.yml` file as well as scripts and config file to bootstrap your installation.

```bash
mkdir -p /opt/sentinelc
cd /opt/sentinelc
git clone https://gitlab.com/sentinelc/docker-compose.git
cd docker-compose
```


## Environment configuration

The env-builder repository works by accepting a few high-level configuration options and generating all the required configuration files from templates.

Edit the `config.sh` file with your favorite editor. Refer to [config file reference](config-reference) for complete details.

## Source your config

Many of the scripts require the variables you just defined in your `config.sh`. Before continuing, source the configs:

```bash
source config.sh
```

## Generate templates

Once all settings in `config.sh` have been set, simply call the `init-environment.sh` script:

```bash
./init-environment.sh
```

Example output where `SC_ENV_NAME` has been set to `my-sentinelc`:

```
Generating random credentials under my-sentinelc/credentials...
 - my-sentinelc/credentials/keycloak
 - my-sentinelc/credentials/vpnrouter_api_token
 - my-sentinelc/credentials/login-instructions.txt

Generating docker-compose environment...
 - Copying docker-compose config files to my-sentinelc/apps/
 - Creating empty volumes
   - my-sentinelc/volumes/certbot/conf
   - my-sentinelc/volumes/certbot/www
   - my-sentinelc/volumes/vpnrouter
 - Generating docker-compose config files from templates
 - Generating keycloak JSON import file from template

Generating wireguard server key pair...
 - Private wireguard key saved to ../volumes/vpnrouter/private_key
 - Public key to import to API server saved to ../volumes/vpnrouter/public_key

docker-compose.yml and supporting files have been created in the my-sentinelc directory.
SUCCESS
```

## Start containers

Your `docker-compose.yml` file as well as supporting configuration files will be located under `$SC_ENV_NAME/apps/`.

Check the output of the `init-environment.sh` script above for confirmation.

Navigate to your environment directory and attempt to start the docker containters:

```bash
cd my-sentinelc  # change this according to your $SC_ENV_NAME
cd apps
docker-compose up -d
```

The first time you run this command, a local `proxy` container will be built and all required containers will be pulled from docker hub. This will take some time.

Once completed, the containers will run in the background. You can confirm this using docker-compose:

```bash
docker-compose ps
```

Example output:

```
Name                     Command               State                     Ports                  
-------------------------------------------------------------------------------------------------------
apps_api_1           /bin/sh -c supervisord -c  ...   Up       80/tcp                                  
apps_api_db_1        docker-entrypoint.sh postgres    Up       5432/tcp                                
apps_api_redis_1     docker-entrypoint.sh redis ...   Up       6379/tcp                                
apps_certbot_1       certbot renew                    Exit 0                                           
apps_front_1         /docker-entrypoint.sh ngin ...   Up       80/tcp                                  
apps_keycloak_1      /opt/jboss/tools/docker-en ...   Up       8080/tcp, 8443/tcp                      
apps_keycloak_db_1   docker-entrypoint.sh postgres    Up       5432/tcp                                
apps_portal_1        /docker-entrypoint.sh ngin ...   Up       80/tcp                                  
apps_proxy_1         /docker-entrypoint.sh ngin ...   Up       0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
apps_vpnrouter_1     /bin/sh -c supervisord -c  ...   Up       0.0.0.0:20208->20208/udp, 80/tcp        
apps_worker_1        python3 manage.py qcluster       Up       80/tcp                                  
```

The certbot container will be stopped (Exit 0). This is normal as certbot is only used to renew SSL certificates, it is not meant to run continuously.

All other containers should remain Up.

If any container is crashing or not starting at this point, you should investigate the problem as there is probably an issue with your setup. Do not attempt to continue with the installation.

## Wait for database initialization

Upon first start, the `api` container will initialize its database or migrate the current schema to the latest version. This takes some time and you should wait for the migrations to complete.

Use the following command to wait for the process to complete:

```bash
./wait-for-migrations.sh
```

Example output:

```bash
Cannot execute silk_profile as silk is not installed correctly.
Operations to perform:
  Apply all migrations: accounting, admin, auth, authtoken, blobs, contenttypes, devices, django_q, firmwares, images, incidents, monitoring, networks, notes, notifications, onboarding, organizations, radius, reversion, secops, sessions, silk, sites, sshkeys, tasks, tenants, vpn, wifi_survey
Running migrations:
  No migrations to apply.
```

When you see the confirmation `No migrations to apply.` the script should return and you are ready to proceed. If there is a problem with your setup, the migrations can fail to apply and the script will retry for 5 minutes.

You can abort the script using `ctrl-c` and check the api container output (`docker-compose logs api`).

## Configure SSL certificates

A self-signed ssl certificate will be generated on the first start of the proxy service.

The certificate and private key is stored under:

```
apps/volumes/ssl/fullchain.pem
apps/volumes/ssl/privkey.pem
```

You will need to obtain a valid certificate for all the domains of the solution (see [dns](DNS)).


## Configure the VPN

An internal wireguard VPN is used to secure communications with SentinelC devices. An extra step is required to finish its configuration:

```bash
docker-compose exec api ./manage.py add_vpn_router \
   --name vpnrouter \
   --public-key `cat volumes/vpnrouter/public_key` \
   --subnet 172.28.0.0/15 \
   --device-subnet 172.29.0.0/24 \
   --privileged-subnet 172.28.16.0/23 \
   --icinga-subnet 172.28.1.4/32 \
   --endpoint-ip $EXTERNAL_IP \
   --token `cat ../credentials/vpnrouter_api_token`
```

This scary looking command configures the VPN server with the random keys and credentials that were generated using the `init-environment.sh` script earlier.

- Do not change the `name` (vpnrouter) option without also changing `ROUTER_NAME` in `vpnrouter.env`.
- The `subnet`, `device-subnet` and `icinga-subnet` options can be changed if you prefer to use another subnet. For example if you are already using 172.28.x.x ou 172.29.x.x internally. Otherwise, these defaults will work fine for most installations.
- The `icinga-subnet` is only for the optional Icinga monitoring integration, but at this moment a value is required. It will have no effect unless the integration is completed (TODO: link to optional icinga docs).
- `endpoint-ip` is the public IP address of your server. You have defined it in `config.sh` as [EXTERNAL_IP](config-reference#external_ip) and we can reuse it.

:::note TODO

A chapter should be added for general information about the VPN and linked here.

:::


## Import keycloak configuration

The `init-environment.sh` also generated a Keycloak configuration file to initialize your keycloak authentification server.

This configuration file needs to be imported to complete the keycloak setup.

```bash
cd keycloak/
./import.sh
```

Example output:

```
2021-07-21 20:38:50.518  INFO 39190 --- [           main] d.a.k.config.KeycloakConfigApplication   : Starting KeycloakConfigApplication v3.4.0 using Java 11.0.11 on sc-install-test1 with PID 39190 (/opt/sentinelc/env-builder/demo1/apps/keycloak/keycloak-config-cli-13.0.0.jar started by root in /opt/sentinelc/env-builder/demo1/apps/keycloak)
2021-07-21 20:38:50.534  INFO 39190 --- [           main] d.a.k.config.KeycloakConfigApplication   : No active profile set, falling back to default profiles: default
2021-07-21 20:38:53.155  INFO 39190 --- [           main] d.a.k.config.KeycloakConfigApplication   : Started KeycloakConfigApplication in 4.717 seconds (JVM running for 6.393)
2021-07-21 20:38:55.765  INFO 39190 --- [           main] d.a.k.config.KeycloakConfigRunner        : Importing file '/opt/sentinelc/env-builder/demo1/apps/keycloak/./sentinelc.json'
2021-07-21 20:39:09.605  INFO 39190 --- [           main] d.a.k.config.KeycloakConfigRunner        : keycloak-config-cli running in 00:14.461.
```


## Create an admin user

Come back to the directory containing your `docker-compose.yml` file (`cd ..`) and execute the following command:

```
docker-compose exec api ./manage.py create_sso_admin --email ${ADMIN_EMAIL} --firstname "${ADMIN_FIRSTNAME}" --lastname "${ADMIN_LASTNAME}"
```

This will allow you to login as an administrator in the application using the ADMIN_EMAIL you selected in your `config.sh` earlier.

Run this command in folder my-sentinelc/apps to configure your domain name properly in the API server:
```
cd..
docker-compose exec api ./manage.py fix_default_site
```

## Verify the system health status

A built-in page can confirm if your installation is fully up and running.

Open https://api.__SC_BASE_DOMAIN__/health/ in your browser.

For example, if you have chosen `sentinelc.example.org` as your domain name, the URL will be: `https://api.sentinelc.example.org/health/`

At this point of the installation process, all system health checks should be OK.
