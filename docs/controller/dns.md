---
title: DNS configuration

---

A valid domain name or subdomain is required to complete the installation.

You can use a top-level domain such as `example.org` or a subdomain such as `sentinelc.example.org`.

This is the domain you will input as the [SC_BASE_DOMAIN](config-reference#sc_base_domain) in the [installation](installation) step.

Once you have chosen your desired domain name, you will need to setup the following DNS records with your DNS provider:

| name      | type | description                        | example FQDN                        |
|-----------|------|------------------------------------|-------------------------------------|
| api.      | A    | API service                        | api.sentinelc.example.org           |
| accounts. | A    | Keycloak service                   | accounts.sentinelc.example.org      |
| app.      | A    | Web application                    | app.sentinelc.example.org           |
| portal.   | A    | Wifi sign-in portal                | portal.sentinelc.example.org        |
| logs.     | A    | Logs/metrics service               | logs.sentinelc.example.org          |
| *.apps.   | A    | Wildcard for managed edge services | abcd1234.apps.sentinelc.example.org |

All records should point to the IP address of your linux server hosting the controller. This IP address is also configured under [EXTERNAL_IP](config-reference#external_ip) in the `config.sh` script.

If you are able to configure the TTL of your records, you can use a value between 300 and 86400 seconds, depending on your preference.

The *.apps wildcard is only required if you intend to deploy services on your managed appliances. If a service offers a web interface, a cloud proxy can be automatically setup using a subdomain.
