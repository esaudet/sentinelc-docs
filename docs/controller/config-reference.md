---
title: Env-Builder configuration reference

---

At the root of the env-builder repository, you will find a default `config.sh` that you need to modify to configure your new installation.

### Common settings

#### SC_DISPLAY_NAME

This is a human friendly name for your environment. It will be used for login pages and email From fields. Use a short name to identify your installation.

#### SC_ENV_NAME

A name suitable to use for a directory name. Use a short, all lowercase, alphanumeric name to identify your installation. Hyphens are allowed but no other punctiations.

#### SC_BASE_DOMAIN

The domain or sub-domain you will use for your installation. You need to be in control of this domain name. You could use a top-level domain name such as `example.org` or a sub-domain such as `sentinelc.example.org`. See [DNS configuration](dns) for more details.

#### EXTERNAL_IP

The publicly reachable IP address of the linux host where you are installing the controller.

### Optional features

#### ENABLE_ONBOARDING

The Onboarding feature is an online wizard that allows an anonymous user to activate SentinelC devices and create an account to manage their device. This feature is suitable if you intend to ship brand new devices to your users and expect them to self-register their devices using a registration key you will provide. This is disabled by default.

Set to `true` to enable the feature.

#### MAINTENANCE_ON

This option can be used to put the frontend and the api in maintenance mode. Defaults to `false`.
See [maintenance mode](maintenance-mode) for more details.

### Outgoing mail server configuration

#### SMTP_HOST

Outgoing mail server address.

#### SMTP_PORT

Outgoing mail server port. Common ports are 25, 465 (SSL) and 587 (TLS).

#### SMTP_USER

Outgoing mail server user. Leave empty to disable authentification.

#### SMTP_PASS

Outgoing mail server password. Leave empty to disable authentification.

#### SMTP_USE_SSL

Set to `true` if your SMTP server requires automatic (implicit) encryption. This is commonly used on port 465.

#### SMTP_USE_TLS

Set to `true` if your SMTP server supports STARTTLS (explicit) encryption. This is commonly used on port 587. This is disabled if SMTP_USE_SSL is enabled.

#### ADMIN_EMAIL

Valid email address where automated messages and security notices will be sent. This is also used for the initial admin user.

#### ADMIN_FIRSTNAME

A first name to use for the initial admin user.

#### ADMIN_LASTNAME

A last name to use for the initial admin user.
