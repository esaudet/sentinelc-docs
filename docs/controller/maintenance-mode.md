---
title: Maintenance Mode

---

Maintenance mode allows you to display a user-friendly web page instead of a broken site during maintenance.
It also allows developers to safely override the maintenance page and access every ressource they need.

### Available settings

#### MAINTENANCE_ON

Set this variable to `true` to activate the maintenance mode. Defaults to `false`.

#### MAINTENANCE_PASS

The password to override the maintenance page for developers. This is randomly generated at first, but you can change it to a password of your liking.

### Activation

Navigate to your environment directory :

```bash
cd sentinelc/docker-compose/my-sentinelc/apps
```


To activate the maintenance mode, you must add these settings in the `api.env` and the `front.env` file :

```bash
MAINTENANCE_ON=true
MAINTENANCE_PASS=secret
```

After changing the value of `MAINTENANCE_ON` to `true`, you can run :

```bash
docker-compose up -d
```

to rebuild the api and front containers with the maintenance mode activated.


### Overriding the maintenance page

It is also possible for developers to override the maintenance page. To do so, they will need to set the `maintenance_pass` cookie to the value of the `MAINTENANCE_PASS` setting.

The best way to set the cookie is to use the javascript console in your favorite browser and enter this code:

```js
document.cookie = "maintenance_pass=secret";
```

You will need to repeat this step for the app (app.yourdomain.com) and also  for the api (api.yourdomain.com).

Once you have done that, you can access every ressource you want in the app.

### Customizing the web page

The maintenance web page is a static html file. You can customize it to your liking. To do so, you need to update the `under_maintenance.html` located in `maintenance/under_maintenance.html` of the `front` project and rebuild the container.

If you don't want to rebuild the container, you can also only edit the `/app/under_maintenance.html` file inside the front container, but please note that all your changes will be overritten if the container is rebuilt in the future.

### Deactivation

To deactivate the maintenance mode, you must put the `MAINTENANCE_ON` setting to `false` in the `api.env` and the `front.env` file :

```bash
MAINTENANCE_ON=false
```

Do not forget to rebuild the containers with the new value :

```bash
docker-compose up -d
```

That's it!
