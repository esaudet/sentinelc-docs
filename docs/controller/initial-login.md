---
title: Initial login

---

Once the installation steps are completed, you should be able to log into your new installation.

You can view the random login credentials for your initial user in the file `credentials/login-instructions.txt`:

For example:

```bash
# cat credentials/login-instructions.txt
Back-end admin site:
https://api.sentinelc.example.org/admin (Use the Single sign-on button)

Front-end application:
https://app.sentinelc.example.org/support


The auto-generated random credentials are:

Username: dev@sentinelc.com
Password: xxx

You will be asked to set a new password after logging in for the first time.


An admin user has also been generated for accessing the Keycloak master realm:

https://accounts.sentinelc.example.org/auth/admin/

Username: admin
Password: xxx

```

Login to admin portal.

![login](./login.png)

After your first connection to admin portal, you can view Inventory account.

![Inventory account](./inventory-account.png)

Now that the cloud controller is operational, it is necessary to register an appliance
