---
title: Add Wi-Fi network
---


## How add a Wi-Fi network?

Click on your location and select the red arrow next to your location name.

![organization](./installation-ap/organization.png)

On the Location page, select **Wi-Fi** in the *Add* section. And in the Wi-Fi page, select **Add a Wi-Fi network**.

![Location](./installation-ap/locations.png)

You will then be redirected to the Wi-Fi network creation page.

![add Wi-Fi](./add-wi-fi/add-wi-fi.png)

Select the type of network you want to create and follow the steps to create it.

Example here of adding a Private Wi-Fi network with the shared key displayed when the Wi-Fi is created.

![Create Wi-Fi](./add-wi-fi/create-wi-fi.png)

Enter the name of your Wi-Fi network and click on Continue.

If no logical zone is present, a Private zone will be created automatically. You will then have to select appliance that will transmit your Wi-Fi network.

![Zone Wi-Fi](./add-wi-fi/zone-wi-fi.png)

Click Apply to create the Wi-Fi network and you will see the key to connect to it.


![Wi-Fi key](./add-wi-fi/wi-fi-key.png)

## View password for my Wi-Fi network

On the Location page, select **Wi-Fi** in the *Add* section.

![Wi-Fi list](./add-wi-fi/wi-fi-list.png)

You will then see the Wi-Fi password and the QR code to connect to it.

![Wi-Fi key](./add-wi-fi/wi-fi-password.png)