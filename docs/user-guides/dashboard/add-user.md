---
title: Add user
---


## How add a user profile?

To add other network user profile.

Go to the **Users** tab, then to the **Create User Profile** menu.

![Add user](./add-user/add-user.png)

Enter the user's information. Finally click Save to register the new user for your SentinelC network.

![Add user](./add-user/add-user-info.png)

An account activation email with a password creation request will be sent to the newly created user's email.