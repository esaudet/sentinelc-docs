---
title: Add location
---


## How to add a second SentinelC router?

It is possible to add multiple locations for the same account, for this you will need a new appliance to set up your new lcoation.

In the SentinelC application, go to your organization's page and click on the menu for **Add a location**.

![Location](./add-location/add-location.png)

Then you just have to enter the information for the **name of your location**, **the identification of the device** and **the product key**. To finish, click on **Install** to complete the configuration of your SentinelC router.

![Location](./add-location/location-info.png)

However, if you want to extend your Wi-Fi network, you can add one or more access points to your location that will allow you to extend the Wi-Fi networks of your choice.

See the section how to [extend Wi-Fi](extend-wi-fi) for more information.