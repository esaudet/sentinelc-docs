---
title: Internet with static IP
---
---

## How to connect to the internet with a static IP?

After you are connected to the [Local web status page](/docs/technical-guides/local-web-status-page) of an appliance.

In the local status page of your appliance, go to the bottom of the section Edit WAN connection.

![static ip](./internet-with-static-ip/static-ip.png)