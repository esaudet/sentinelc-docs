---
title: Getting started
---


## Appliance

After selecting an appliance and [flashing the SentinelC OS](/docs/technical-guides/installation-os#architecture-arm-rpi4), the user must perform onboarding of the appliance in order to setup the network.

## Onboarding

[Onboarding](./sentinelc-router-initial-onboarding) is done in a few minutes to set up an account, an administrator profile, a location, one or more Wi-Fi networks.

The user can define several locations on a single account.

A confirmation email is sent to the user to set up his password and finalize the installation.

## Connection to the SentinelC dashboard.

After completing the installation, log in to the SentinelC dashboard: [https://app.sentinelc.com](https://app.sentinelc.com).

In home page you can see your complete network, users and devices connected to the network.

![organization](./installation-ap/organization.png)

Select your location to manage your network. You can then see:

- Zones.
- Appliances.
- Wi-Fi network
- Connected devices.
- Network activity.
- Incident history.

You can also add:

- Wi-Fi network.
- Acces point.
- Drone.

![Location](./installation-ap/locations.png)