---
title: Installation access point
---


## How to install and connect an access point?

### Step 1 - Configure your ports.

First check if you have any ports available. Click on your location and select the red arrow next to your location name.

![organization](./installation-ap/organization.png)

On the Location page, select **Add Access Point** in the *Add* section.

On the location page, select **Appliances**.

![Location](./installation-ap/locations.png)

On the appliance page, click on **Edit** next to the appliance name to edit it.

![Appliances edit](./installation-ap/appliances-edit.png)

Then go to the bottom of the appliance page, click on **Edit** and then select a drop-down list on the port you want to assign your access point to, and select **Access Point**.

![Appliances edit port](./installation-ap/appliances-edit-port.png)

Click on **Apply** and then in the popup, click on **Ok** to save your changes.

### Step 2 – Physically plug in the device to authorize it on your network.

Once the port is selected and configured, please connect your access point to this port.

Then you need to authorize the device to connect to the network. To do so, go back to the location page of your network.

You will receive an authorization request: Waiting for permission.  Click on the grey arrow on the right of the Devices section.
