---
title: Firmware update
---


## How to manage a firmware update?

Connect to SentinelC dashboard and click to the user menu.

![User menu](./firmware-update/user-menu.png)

Select Settings.

![User settings](./firmware-update/user-settings.png)

Then go to the Router firmware update section.

![Firmware update](./firmware-update/firmware-update.png)

You will have a drop-down list with 3 choices:

- **Automatic**: the update will be done between 2am and 3am.
- **Immediate**: the update will be done immediately when it is made available.
- **Custom**: you can choose the update time of your choice.

![Firmware update](./firmware-update/firmware-update-list.png)