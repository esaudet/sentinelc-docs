---
title: Wi-Fi schedule
---


## How schedule management?

Click on your location and select the red arrow next to your location name.

![organization](./installation-ap/organization.png)

On the Location page, select **Wi-Fi** in the *View* section.

![Location](./installation-ap/locations.png)

Select the Wi-Fi where you want to enable schedule management and click on **Schedule**.

![Wi-Fi schedule](./wi-fi-schedule/wi-fi-schedule.png)

You are now in the Wi-Fi Schedule page, by default the schedule management is disabled.

![Wi-Fi schedule management](./wi-fi-schedule/wi-fi-schedule-management.png)

To **activate** the schedule management, click on the button to the right of **Schedule Management**.

Then select the day you want to modify its schedule.

![Wi-Fi schedule management edit](./wi-fi-schedule/wi-fi-schedule-management-edit.png)

You then have 4 choices available to you:

- Open 24 hours: the Wi-Fi network remains open at all times.
- Closed all day: the Wi-Fi network is closed and no longer emits any signal.
- Schedule: you have the choice to select the opening and closing time of the Wi-Fi network and one or more intervals.
- Same as...: you can choose to have Monday's time be the same as Tuesday's time.

Click on Apply to save your schedule changes.
