---
title: Port status
---


## How to know the status of the ports on my SentinelC device?


To find out the status of your device's ports, log into the SentinelC application, select your location.

On the location page, select **Appliances**.

![Location](./installation-ap/locations.png)

On the appliance page, click on **Edit** next to the appliance name.

![Appliances edit](./installation-ap/appliances-edit.png)

In the appliance's information page, at the bottom you will see the number of available and used ports, their status will appear depending on the current configuration. A legend appears to better describe their status.

![Ports status](./ports-status/ports-status.png)

- Online port status: allows you to see that the device is connected to the network on the correct port.

- Offline port status: this means that the device is disconnected from the equipment or it is turned off. The device may also be on but in standby or it is not responding.
If no other device is connected, it means that the device was the last device connected in an offline port, to know the date of last connection, just click on the i in the blue bubble to the right of the device name.

- Closed port status: a port can be blocked if a bad connection is made.
