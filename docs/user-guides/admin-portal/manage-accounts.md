---
title: Manage accounts
---

The accounts page of the Admin portal is used to manage all SentinelC accounts.

It is possible to search for an account when there are several accounts.

To get more information, just click on the small triangle to the left of the account name to access the detailed information for that account.

![Manage-accounts](manage-accounts/manage-account.png)

## How to create a account?

To create an account, click on the red **Create Account** button at the top right.

Then you just have to fill in the form with the account and contact information in order to create the account.

![Manage-accounts](manage-accounts/create-account.png)