---
title: Embedded services
---

An **embedded service** refers to a particular instance of a service from the 
[service library](/docs/user-guides/admin-portal/manage-service-library).

## Service statuses

Service statuses are displayed on the Embedded services page and the appliance details page.
This status is high-level and is used to manage the life cycle of the service.
Each service can have one of the following statuses:

| Status        | Description                                         |
|---------------|-----------------------------------------------------|
| Available     | The service is installed and ready to take actions. |
| Not available | The service is not available to take actions.       |
| Error         | The service has failed.                             |

In addition to the main service status, a current state is also shown to give more information
about the actual status of the service.
Each service can have one of the following current statuses:

| Current Status           | Description                                                       |
|--------------------------|-------------------------------------------------------------------|
| Ready                    | The service has been installed and is ready to start.             |
| Running                  | The service is alive and currently running.                       |
| Stopped                  | The service has been stopped by the user.                         |
| Missing                  | The service has not been reported by the appliance.               |
| Uninstalling             | An uninstall task has been sent to the appliance.                 |
| Stopping                 | A stop task has been sent to the appliance.                       |
| Starting                 | A start task has been sent to the appliance.                      |
| Installing               | An install task has been sent to the appliance.                   |
| Updating parameters      | An update parameters task has been sent to the appliance.         |
| Service failed           | The appliance has reported that the service has failed.           |
| Start failed             | A start task has failed.                                          |
| Stop failed              | A stop task has failed.                                           |
| Install failed           | An install task has failed.                                       |
| Uninstall failed         | An uninstall task has failed.                                     |
| Update parameters failed | An update parameters task has failed.                             |
| Unknown                  | The appliance is offline or the service [disk](disks) is missing. |

![services-statuses](img/embedded-services/service-all-statuses.png)


## Service actions

Each service has action buttons enabled or disabled in relation to the current state of the service:

![services-actions](img/embedded-services/services-actions.png)

These actions are start, stop, uninstall, show parameters and link to web based interface respectively.

In addition to these actions, you can also:

- Install a new service
- Force stop all running services

with the help of the service component menu actions in the appliance details page:

![services-actions](img/embedded-services/services-component-actions.png)


### Install

#### Prerequisites

1. An appliance configured in a location.
2. At least one service available from the service library.
3. A [disk](disks) configured for services on the appliance.
4. A zone configured in the current location.

#### Service component

The installation of a service is launched from the services component of the appliance details page:

![services-component](img/embedded-services/services-component.png)

Then, you can expand the component menu and click on **Install a service** action button:

![service-instance-install](img/embedded-services/service-instance-install.png)

#### Service selection
Now you will see all the available services from the service library. You need to select the service
that you want to install and the zone inside which the service will operate:

![service-instance-choose-service](img/embedded-services/service-instance-install-choose-service.png)

Next, click continue. 

#### Service parameters
If the service has parameters, a popup will show up and with all the configurable parameters:

![service-instance-choose-service](img/embedded-services/service-instance-install-parameters.png)


The first section shows all the **ports that are exposed** by the service. 
It is often a http port to expose a web interface to manage directly the service.
You can choose between 2 options:

| Option                   | Description                                                                |
|--------------------------|----------------------------------------------------------------------------|
| allow proxy cloud access | Make the port accessible from the cloud by a randomly generated http link. |
| allow local access       | Make the port accessible directly into the configured zone.                |

Each option can be checked or unchecked as desired by the user.

The second section shows all the other parameters that can be set up by the user e.g. the desired timezone in the above screen capture.

**Required parameters have an asterisk (*) and must be filled before clicking the ok button.**

Once parameters are correctly set, you can click the ok button to start the installation.

Once the installation is done, you should see the service with the current state **Ready** in the 
services component:

![service-instance-choose-service](img/embedded-services/service-instance-install-ready.png)

### Start

#### Prerequisites
1- An installed service on an appliance (see [install](#install) section for details)

#### Service component
The start action of a service is initiated from the action buttons of the services component.

![service-instance-choose-service](img/embedded-services/service-start.png)

#### Current statuses
You can only start the service when it is in one of the following current statuses:
- Ready
- Stopped
- Recent failed actions:
  - Start failed
  - Update parameters failed
  - Uninstall failed
- Service failed

#### Task
Once you click the start button, a start task is sent to the appliance and you should see the current status
change to **starting**. 

If the start works, the service current status should change to **Running**.
If the start fails, the service current status should change to **Start failed** and you can see 
the reason by checking task details in the tasks component.

### Stop

#### Prerequisites
1- A running service on an appliance (see [start](#start) section for details)

#### Service component
The stop action of a service is initiated from the action buttons of the services component.

![service-instance-choose-service](img/embedded-services/service-stop.png)

#### Current statuses
You can only start the service when it is in one of the following current statuses:
- Running
- Recent failed actions:
  - Stop failed
  - Update parameters failed

#### Task
Once you click the stop button, a stop task is sent to the appliance and you should see the current status
change to **stopping**. 

If the stop works, the service current status should change to **Stopped**.
If the stop fails, the service current status should change to **Stop failed** and you can see 
the reason by checking task details in the tasks component.

### Show parameters

#### Prerequisites
1- An installed service on an appliance (see [install](#install) section for details)

#### Service component
The show parameters action of a service is available from the action buttons of the services component.

![service-instance-choose-service](img/embedded-services/service-show-parameters.png)

Once you click the show parameters button, a popup will show up with all the current parameters 
of the service:

![service-instance-choose-service](img/embedded-services/service-show-parameters-popup.png)

From there, you can also edit the parameters by clicking the edit button. The form will make editable
all parameters that can be changed after the initial installation. If you cannot edit one of the parameters,
it is because this parameter cannot be changed after the initial installation.

#### Task
If you save the parameters' edition form, an update parameters task is sent to the appliance and you should see the current status
change to **Updating parameters**. 

If the parameters update works, the service current status should change to his initial status (before you edit the parameters).
If the parameters update fails, the service current status should change to **Update parameters failed** and you can see 
the reason by checking the task details in the tasks component. 

**Important note**: In case of failure, the show parameters form will show the updated parameters
but not the parameters that was set before the parameters update failure. If the service is still 
running, it is the old parameters that are applied and not the newest ones. A warning icon is shown if that
situation occurs and invite the user to retry the update parameters action:

![service-instance-choose-service](img/embedded-services/service-show-parameters-warning.png)

### Uninstall

#### Prerequisites
1- An installed service on an appliance (see [install](#install) section for details)
1- A stopped service on an appliance (see [stop](#stop) section for details)

#### Service component
The uninstall action of a service is available from the action buttons of the services component.

![service-instance-choose-service](img/embedded-services/service-uninstall.png)

#### Current statuses
You can only start the service when it is in one of the following current statuses:
- Missing
- Stopped
- All recent failed actions

#### Task
Once you click the uninstall button, an uninstall task is sent to the appliance, and you should
see the current status change to **Uninstalling**. 

If the uninstall works, the service will disappear from the list.
If the uninstall fails, the service current status should change to **Uninstall failed** and you can see 
the reason by checking task details in the tasks component.
