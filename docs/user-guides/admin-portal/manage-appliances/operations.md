---
title: Appliance operations
---

To perform an action on one or more appliance, go to the [Appliances](listing-appliances) section of the [Admin portal](/docs/user-guides/admin-portal/access).

Check the box of the appliance on which you want to perform an action.

![Appliance action](img/actions.png)

Then click on the Action drop-down menu and select the action you wish to perform. Click on Apply to start the task.

![Appliance action](img/actions-list.png)

Several actions are available:

- [Reboot](#reboot-an-appliance)
- [Start Wi-Fi survey](#start-wi-fi-survey)
- [Schedule update](#schedule-update)
- [Immediate update](#immediate-update)
- [Transfer to...](#transfer-to)
- [Factory reset](#factory-reset)
- [Return to operator](#return-to-operator)

Operations on appliances use the [Tasks](/docs/user-guides/admin-portal/manage-tasks) system. Tasks are processed asynchronously and you can monitor their progression and cancel them. This allows you to perform operations even if an appliance is currently offline.

### Rebooting an appliance

After selecting your appliance, use the **Reboot** action and click **Apply**.

![Reboot action](img/reboot.png)

You will then see that the request has been successfully submitted. Click on **View Task** to verify that the requested action is in progress.

![Reboot action apply](img/reboot-apply.png)

You are then redirected to the tasks page with the current appliance reboot task.

![Reboot action pending](img/reboot-pending.png)

### Start Wi-Fi survey

A [Wi-Fi survey](/docs/user-guides/admin-portal/wi-fi-scans) is a quick scan of in-range SSID/BSSID. This allows detecting wi-fi neighbours.

After selecting your appliance, use the **Start Wi-Fi survey** action and click **Apply**.

![Wi-Fi survey action](img/wi-fi-survey.png)

You will then see that the request has been successfully submitted. Click **View Task** to verify that the requested action is pending, in progress or completed.

![Wi-Fi survey action apply](img/wi-fi-survey-apply.png)

You will be redirected to the task page with the pending, in progress or completed Wi-Fi Scan task for the appliance.

![Wi-Fi survey action pending](img/wi-fi-survey-pending.png)


### Schedule update

This operation schedules the firmware update, if there is one available, during the next maintenance window of the network location.

After selecting your appliance, use the **Schedule Update** action and click **Apply**.

![Schedule update](img/schedule-update.png)

You will then see that the request has been successfully submitted. Click on **View Task** to verify that the requested action is pending, in progress or completed.

![Schedule update apply](img/schedule-update-apply.png)

You are then redirected to the tasks page with the pending device firmware update task. The update will be triggered according to your update preferences.

![Schedule update pending](img/schedule-update-pending.png)

### Immediate update

An immediate update is the same as a scheduled update, except that the maintenance window is ignored and the update will start as soon as possible.

After selecting your appliance, use **Immediate update** action and click **Apply**.

![Immediate-update](img/immediate-update.png)

You will then see that the request has been successfully submitted. Click on **View Task** to verify that the requested action is in progress or completed.

![Immediate-update](img/immediate-update-apply.png)

You will be redirected to the tasks page with the current or pending device firmware update task. The update will take place immediately.

![Immediate-update](img/immediate-update-pending.png)

### Transfer to...

After selecting the not in use appliance you wish to transfered, use the ****Transfert to**** action and click **Apply**.

Note: to transfer an appliance, it must be without a location.

![Transfert to](img/transfert-to2.png)

A confirmation message will appear to confirm the transfered device from account selected.

![Transfert to confirm](img/transfert-to2-confirm.png)

You will then see that the request has been successfully submitted.

![Transfert to apply](img/transfert-to2-apply.png)

### Factory reset

After you have selected the appliance you wish to reset, use the **Factory reset** action and click **Apply**.

![Factory reset](img/factory-reset.png)

A confirmation message will appear to confirm the device to factory reset

![Factory reset](img/factory-reset-confirm.png)

You will then see that the request has been successfully submitted. Click on **View Task** to verify that the requested action is pending, in progress or completed.

![Factory reset](img/factory-reset-apply.png)

You are then redirected to the tasks page with the device reset task in progress or pending.

![Factory reset](img/factory-reset-pending.png)

Once finished, the appliance appears **Not in use** in the list of appliances, it no longer has a location but it still belongs to the account

![Factory reset](img/factory-reset-not-use.png)

### Return to operator

After selecting the not in use appliance you wish to return to operator, use the **Return to operator** action and click **Apply**.

Note: the return to operator is only possible if the appliance is **Not in use**.

![Return to operator](img/return-operator.png)

A confirmation message will appear to confirm the device to return to operator.

![Return to operator](img/return-operator-confirm.png)

You will then see that the request has been successfully submitted.

![Return to operator](img/return-operator-apply.png)

Go to the Inventory section to see the appliance that is returned.

![Return to operator](img/return-operator-returned.png)
