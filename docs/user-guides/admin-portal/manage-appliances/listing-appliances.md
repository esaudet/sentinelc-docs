---
title: Listing appliances
---

The **Appliances** section of the admin portal is used to manage all SentinelC routers or access points.

There are two main listing page available:

## Appliances

This lists all the currently configured appliances. A configured appliance is one that is under active duty, in a Network location and is owned by an Account (tenant).

![Menu](img/manage-appliances-menu.png)

## Manage inventory

This lists the appliances that are newly registered with the cloud controller, that are in stock or that have been returned to the inventory.

All appliances will appear in the Manage inventory section first when you initially [register them to the cloud controller](/docs/technical-guides/local-web-status-page).

In order to configure a device that is currently in the inventory, use the Claim operation.

![Buttons](img/inventory-buttons.png)

See the [Inventory](inventory) section for all details.
