---
title: Disks
---

Additional disks can be inserted in an appliance mainly to install [embedded services](
embedded-services/).
When inserted, the disk is automatically recognized by the appliance and the user is 
invited to format it and assign a particular usage.

## Disk statuses

Disk statuses are displayed on the disks component of the [appliance details page](appliance-details/).
This status is used to represent the hardware status of the disk.
Each disk can have one of the following statuses:

| Status        | Description                                                     |
|---------------|-----------------------------------------------------------------|
| Available     | The disk has been reported by the appliance. (Plugged-in)       |
| Missing       | The disk has not been reported by the appliance. (Disconnected) |

In addition to the main disk status, a current state is also shown to give more information
about the filesystem status of the disk.
Each disk can have one of the following current statuses:

| Current Status | Description                                                          |
|----------------|----------------------------------------------------------------------|
| Ready          | The disk has been formatted at least once.                           |
| Unformatted    | The disk is new and has not been formatted yet.                      |
| Error          | The appliance has reported errors (data corruption, bad block, etc.) |
| Unknown        | There is no current status (e.g. for the Missing status)             |


![disk-statuses](img/disks/disks-statuses.png)

## Disk usages

| Usage            | Description                                                   |
|------------------|---------------------------------------------------------------|
| Operating system | The disk is used for the appliance main operating system.     |
| Services         | The disk is used for [embedded services](embedded-services/). |
| None             | The disk has no usage.                                        |


## Disk actions

There are 2 disk actions the user can do:
1. [Format](#format)
2. [Clear](#clear)

### Format

The format action is available for disks that are **not currently in use**. A disk can be used for  [embedded services](
embedded-services/), for the appliance main **operating system** or if a task is currently running on that disk.
In these cases, the format action **will not be available**.

#### Format types
There are 2 types of format: A **quick format** and a **full format**. The quick format only rewrites the 
filesystem while the full format securely wipes all the data before writing the new filesystem. The full format
is more secure but is a lot more time-consuming than the quick.

#### Prerequisites

1. An appliance configured in a location.
2. A disk plugged in the appliance.

#### Disk component

The disk format is launched from the disks component of the [appliance details page](appliance-details/):

![disks-component](img/disks/disks-component.png)

Then, you can click on the format button at the end of the line of the disk you want to format:

![disk-format-button](img/disks/disk-format-button.png)

Once you have clicked, a popup will show up, and you will need to select the format 
type and usage that you want for that disk:

![disk-format-popup](img/disks/disk-format-popup.png)

**Tip**: The None usage is useful when you want to remove the disk's current usage. Some usage like
**Services** only allow one disk at a time with that usage.

#### Task
Once you click the format button, a format disk task is sent to the appliance, 
and you should see the current status change to **Format pending** and then **Format in progress**. 

If the format works, the disk current status should change to **Ready** and the usage to 
the one you have specified. If the format fails, the service current status should stay the same, 
and you can see the reason by checking task details in the tasks component.

#### Embedded services
If you chose the **Services** usage, you now can install new services on the appliance. 
See [embedded service installation](/docs/user-guides/admin-portal/manage-appliances/embedded-services/#install) for more details.


### Clear

The clear action only appears if the disk is in the **Missing** status. Its purpose is to clean
the list of disks when the user really want to remove a particular disk from the list. By declaring 
the missing disks, the user is always aware of which disk have been plugged in or disconnected from 
the appliance.

#### Prerequisites

1. An appliance configured in a location.
2. A disk that was previously plugged in the appliance, unplugged (in Missing status).


#### Disk component

The clear disk action is launched from the disks component of the 
[appliance details page](appliance-details/):

![disks-component](img/disks/disks-component-missing.png)

Then, you can click on the clear button at the end of the line of the disk you want to format:

![disk-format-button](img/disks/disk-clear-button.png)

Once you have clicked, a popup will show up, and you will need to confirm that you really want to 
clear that disk by clicking again on the clear button:

![disk-format-popup](img/disks/disk-clear-popup.png)
