---
title: Port scans
---

Port scans is used to search for open ports on the network, which could be security holes.

Port scanning is performed on TCP and UDP ports and can detect device OS.

The results of port scans are visible in the Admin portal:

![Port scans result](port-scans/result-port-scans.png)

## Perform a port scan on a device connected to the SentinelC network.

To perform a port scan, log into the Admin portal, go to the Monitoring menu and select **Port Scans**. Then click on the red **New Port Scan** button at the top right of the page.

![Port scans result](port-scans/port-scans.png)

Then fill in the form fields to target the device.

![Port scans result](port-scans/new-port-scans.png)

The scan can be performed with several settings: OS detection, TCP port scan and UDP port scan.

Note: The scan on all UDP ports can be quite long.

A reference link to the [NMAP documentation](https://nmap.org/book/man.html) page will help you understand the different types of ports.

Nmap is a port scanner. It is designed to detect open ports, identify hosted services and obtain information about the operating system of a remote device.

Once you have completed the form, click **Apply**.

You are then redirected to the tasks page with the pending or running device port scan task.

![Port scans result](port-scans/port-scans-pending.png)

When the scan is complete, return to the scans page of the Surveillance menu. Click on the small arrow to the left of the scan to view the scan details.

![Port scans result](port-scans/port-scans-details.png)