---
title: Manage administrators
---

## How to manage administrator on your network?

To manage SentinelC network administrators, simply click on the icon at the top of the menu on the left of the portal admin.

![Manage-administrators](manage-administrators/manage-administrators.png)

Here you can see a list of all privileged and non-privileged administrators on your SentinelC network. The results can be sorted by accounts, privileged permission and status.

You can create, edit a user as well as view the entire change history of a user profile.

To edit a user profile, click on the pencil to the left of their email address.

![Manage-administrators-details](manage-administrators/manage-administrators-details.png)

## Create a network administrator.

Click on the red button at the top right **Create new admin profile**.

Enter the user's information. 

![Create-administrator](manage-administrators/create-administrator.png)

You can enable the **privileged permission** and add one or more access profiles for the user to access one or more accounts in your SentinelC network.

![Create-administrator-privileged](manage-administrators/create-administrator-privileged.png)

Finally click on the red **Create Profile** button to save the profile.

## View all change history.

To see the change history of a network administrator account, click on the red button on the right **View all change history**.

![View all change history](manage-administrators/history.png)