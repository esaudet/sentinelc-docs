---
title: Accessing the admin portal
---

The admin portal is accessible under the `/support` path.

For example, if your cloud controller is installed with the domain name `sentinelc.example.org`, the URL of the admin portal will be:
https://app.sentinelc.example.org/support

The admin portal will display all the resources your currently logged in user has access to.

![Admin portal](admin-portal/admin-portal1.png)

If you have a fresh installation, you will login the cloud controller with your [initial admin user account](/docs/controller/initial-login). The admin portal will display the default INVENTORY account that receives ownership of newly registered appliances.

The admin portal allows you to:

- [Manage accounts](manage-accounts). Also known as tenants or organizations. Accounts "own" all other resources and you need to create at least one to configure your SentinelC appliances.
- [Manage user access](manage-administrators). Invite additional users to your platform, grant and revoke permissions on accounts.
- [Manage locations](manage-locations). Location are physical location where your networks are located. They are also known as Networks. They belong to a single Account (owner). You will need to create at least one.
- [Manage appliances](manage-appliances/listing-appliances). Once registered with the cloud controller, your SentinelC appliances will be listed and can be remotely controlled using this section.
- TODO: Embedded services
- TODO: Tasks
- TODO: Projects
- TODO: Service library
- [Manage devices](manage-devices). Monitor client devices that connect to the networks.
- [Manage network connections](manage-connections). Monitor connections from client devices.
- [Manage incidents](manage-incidents). When anomalies are detected by the cloud controller, incidents will be created and can be managed here.
- Additionally, several types of monitoring and scans can be performed:
    - [Packet captures](./packet-captures)
    - [Wi-Fi scans](./wi-fi-scans)
    - [Port scans](./port-scans)
