---
title: Manage devices
---

## How to view devices on your network?

The devices page of the Admin portal is used to manage all devices that connect to the SentinelC network. 

Several filters are available, such as the device category or the connection area.

It is possible to search for a device by its name, MAC address or to search for the account on which the devices are connected.

![Manage-devices](manage-devices/manage-device.png)

To get more information, just click on the small triangle to the left of the device's name to access its detailed information.

![Manage-devices-details](manage-devices/manage-device-details.png)