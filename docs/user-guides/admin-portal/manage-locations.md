---
title: Manage locations
---

The Locations page of the Admin portal is used to manage all SentinelC account locations.

It is possible to search for a location when there are multiple locations.

To get more information, just click on the small triangle to the left of the location's name to access its detailed information.

![Manage-locations](manage-locations/manage-location.png)

## How to create a location?

To create a location, click on the red **Create a new location** button at the top right.

Then you just have to fill in the form with the account and contact information in order to create the new location.

![Create-locations](manage-locations/create-location.png)