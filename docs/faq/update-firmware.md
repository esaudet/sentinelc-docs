---
title: Update firmware
hide_table_of_contents: true
---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### How do I check if my router is up to date?

To check if your router is up to date, log into the SentinelC application, go to your location in the **Appliance** tab.

Your appliance indicates that it is up to date next to its connection date. You can also see it by clicking on **Edit** to check the status of the installed version.

### When are the updates for my router scheduled to install?

By default, the router updates are done automatically and are scheduled to install between 2am and 3am when an update is made available.

To change the update times, view [How to manage a firmware update?](/docs/user-guides/dashboard/firmware-update) section.

</div>