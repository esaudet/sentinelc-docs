---
title: Web Mobile Application
hide_table_of_contents: true
---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### How to connect to the application?

Go to [https://app.sentinelc.com](https://app.sentinelc.com) and click on **Login** or click on **Login** at the top right of the site.

Enter your username and password to log in. If you don't have one, ask the administrator to create an account for you first. If you are already an administrator of your SentinelC account, use your username and password created during the installation of your SentinelC router.

### Why can't I log into the SentinelC application?

Verify that your account exists with the SentinelC account administrator.
If your account exists or if you are the SentinelC account administrator, verify that you have entered the correct email address and password.

If you have forgotten your password, you can reset it by clicking on the **Forgot password**? 
You will then be asked to enter your email address, which is your username, in order to email a password reset link.

If the application website is not accessible, it may be under maintenance for a short time.

### Why can't I add a Wi-Fi network in the application?

You have probably reached the maximum number of Wi-Fi networks created in the application.

There can be no more than **6** Wi-Fi networks of any type per location

For more information, read the section [When I add a Wi-Fi, some choices are grayed out and I cannot add them. Why can't I add them?](./wi-fi-network-internet#when-i-add-a-wi-fi-some-choices-are-grayed-out-and-i-cant-add-them-why-cant-i-add-them)

### What happens if the SentinelC application stops responding?

The application may be undergoing maintenance. If you are unable to reach the [https://app.sentinelc.com](https://app.sentinelc.com) application from your cell phone, please contact the technical team at info@sentinelc.com.

### Where can I change my SentinelC application user password? 

You have 2 possibilities to change your password:

- On the login page of the application, you can reset it by clicking on Forgot password link you will then be asked to enter your email address which is your username, in order to email a password reset link.
- Once logged into the application, you can change your password in the account menu by clicking on **Modify password**.

Your password must :
- be at least 12 characters long.
- include one upper and one lower case letter.
- include one number and one special character.


</div>