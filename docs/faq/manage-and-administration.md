---
title: Manage and administration

hide_table_of_contents: true
---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### What does it mean to block a device?

With the SentinelC router, you have the possibility to block a device. This means that the device will no longer be able to connect to your Wi-Fi network.

You can also block a device if you judge that it consumes too much data.

### How can I retrieve the password for my Wi-Fi network?

In order to find the password of your Wi-Fi network, you must connect to the application and go to the page that lists your Wi-Fi, refer to [Add Wi-fi network](/docs/user-guides/dashboard/add-wi-fi#view-password-for-my-wi-fi-network).

### Can I appoint/add other users to manage my network?

Yes you can add more administrators. To do this refer to [Add user](/docs/user-guides/dashboard/add-user).

### How do I block/unblock a device on the network?

You can block or unblock a device on your network by logging into the SentinelC application. To do this refer to [Blocking device network](/docs/user-guides/dashboard/blocking-device-network).

### How to activate the schedule management?

You can activate the schedule Wi-Fi on your network by logging into the SentinelC application. To do this refer to [Wi-Fi schedule](/docs/user-guides/dashboard/wi-fi-schedule).

### How can I tell when a device is trying to connect to my Wi-Fi network?

When a device connects to your Wi-Fi network in an open area, a notification is sent telling you that a new device is connected to your network.

You are then notified in the SentinelC application in the **Notification** section.

</div>