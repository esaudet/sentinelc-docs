---
title: Administrative support

---

import TOCInline from '@theme/TOCInline';


<TOCInline toc={
    toc
  } />

<div className="questions-title">

### What is the purpose of the Administrative Dashboard and how do I access it?

The administrative dashboard or portal admin is operated by platform administrators but can also be used by support teams who need to manage a SentinelC fleet.

Support teams can view information and perform operations based on access/delegation on installed accounts. The console offers the following features:

- View the list of accounts and their status.
- View the list of locations.
- View the list of devices and their status.
- View the list of devices connected to the network.
- Consult the list of connections to the network.
- Consult and manage the list of incidents: possibility of assigning an incident, or closing it once it has been resolved.

This portal admin will also allow you to manage the incidents triggered on the network, to have a history of them, to resolve them and close them once they are treated by the team.

In order to access the portal admin, please connect to the SentinelC application.

Once logged in, please click on the **Admin portal** link above your account. You can also find this link at any time in the hamburger menu of the application. for more details, refer to [Admin portal](/docs/user-guides/admin-portal/access).

### Why can't I do a port scan with an access point?

It is not possible to perform a port scan from an access point, it is necessary to go through a SentinelC router. Appliances must have an IP address in the client zone to be able to perform a scan.

The access points do not assign themselves an IP address in all zones.

### Who has access to the project menu?

A privileged user can access the project menu in the admin portal. If the administrator is not privileged then he/she will not be able to access the page that lists the projects.

### Why I can't remove an appliance from the project?

An appliance that has already been used for a port scan or packet capture cannot be removed. It is allowed to remove an appliance if no data/file has been generated.
It is allowed to remove an appliance by removing it from its slot, performing a factory reset. It will then appear as removed in the project, but the data of the project will be kept.

### How to launch a packet captures?

To start or modify a packet capture, you need to log in to the portal admin, go to the Monitoring tab and click on the red New Packet Capture button. for more details, refer to [Packet captures](/docs/user-guides/admin-portal/packet-captures#how-to-launch-a-packet-capture).

### I want to monitor a device connected to a private zone.

To monitor a device that is connected to the SentinelC network, you must first identify the area in which the device is connected and its MAC address or name. Once identified in the admin portal, you can perform a packet capture of it, to do so refer to [Packet captures](/docs/user-guides/admin-portal/packet-captures#devices-page).

### I want to capture DNS queries from a device.

To perform a packet capture, log into the portal admin, go to the Monitoring menu and select Packet Capture. Then click on the red New Packet Capture button at the top right of the page, to do so refer to [Packet captures](/docs/user-guides/admin-portal/packet-captures#devices-page).

Then fill in the fields in the form to target the device and the port on which the capture will be performed. For a DNS query capture, enter port 53 in the Wireshark capture filter. Click Apply to start the packet capture.

### How do I add an appliance to the inventory?

To add equipment to the inventory, go to the equipment inventory page of the portal admin.

Select the equipment you wish to add to the inventory, only equipment with the status new, returned or defective can be used, to do so refer to [Registering an appliance](/docs/user-guides/admin-portal/manage-appliances/inventory#registering-an-appliance-to-the-inventory).

### How do I view the history of an appliance?

To view the history of a piece of equipment, go to the Equipment page of the portal admin.

Then select the equipment you wish to view, to do so refer to [Registering an appliance](/docs/user-guides/admin-portal/manage-appliances/inventory#registering-an-appliance-to-the-inventory).


### How to create a network administrator with privileged permission?

To create a network administrator, log in to the portal admin and go to the Manage network administrators section,to do so refer to [Create a network administrator.](/docs/user-guides/admin-portal/manage-administrators/#create-a-network-administrator).

### What is the difference between a privileged administrator and an administrator of one or more accounts?

A privileged administrator can do everything and see on all accounts and their dependencies.

An administrator (authorized access to one or more accounts), has his perimeter limited to his own access. This means that this administrator can do all the actions/views on the accounts he has access to.

For example, if an administrator who has access to accounts A and B tries to modify the profile of another administrator who has access to A, B and C, he will not be able to see that this administrator has access to account C because it is not in his scope. He also has access to the list of incidents in his perimeter only but he cannot update them.

### What happens when I remove an access point?

When you want to remove an access point, go to the equipment section of the portal admin.

After selecting the equipment you want to remove, use the **Factory reset** action and click **Apply**, to do so refer to [Factory reset](/docs/user-guides/admin-portal/manage-appliances/operations#factory-reset).

Once the access point is removed or reset, it is no longer visible. Only its old connections to devices remain visible, as well as its actions and incidents.

### What happens when I delete a master router?

When you want to delete the main router, you have to go to the equipment section of the portal admin.

After selecting the equipment you want to reset, use the **Factory reset** action and click **Apply**, to do so refer to [Factory reset](/docs/user-guides/admin-portal/manage-appliances/operations#factory-reset).

Once the primary router is removed or reset, this will result in a complete deletion of the location and all actions related to it, including incidents.

The primary router is therefore no longer visible.

</div>
