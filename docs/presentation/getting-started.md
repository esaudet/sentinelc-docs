---
title: Getting started

---

![Getting-started](./getting-started.jpg)



## Technical requirements

What you will need?

- Internet connexion.
- The SentinelC [Cloud Controller](/docs/controller/intro) installed on an internet connected server or [VM](/docs/technical-guides/installation-os#configure-on-virtual-machine).
- Flash SentinelC OS on supported devices:
    - [ARM RPI4](/docs/technical-guides/installation-os#architecture-arm-rpi4).
    - [Intel AMD](/docs/technical-guides/installation-os#architecture-intelamd).
    - [Virtual Machine](/docs/technical-guides/installation-os#configure-on-virtual-machine).

---

## Steps

1- Install the [Cloud Controller](/docs/controller/intro) on a server or virtual machine. (40min)

2- [Login](/docs/controller/initial-login) to your cloud controller.

3- [Register your compatible device(s)](/docs/technical-guides/appliance-register-inventory) with the controler.

4- Manage one to many networks.

An administrator or operator can manage several networks.

- Network are defined within an [account](https://www.sentinelc.com/faq/installation/pourquoi-aie-je-besoin-de-creer-un-compte-utilisateur/) and a [location](https://www.sentinelc.com/faq/installation/que-signifie-emplacement-dans-votre-application/).
- Each location of an account can be configured with SentinelC appliances, such as a main router, acces point, or prove.
- An account can have multiple network locations. Each location can be segmented into [zones](https://www.sentinelc.com/faq/wi-fi-reseaux-internet/que-signifie-une-zone-logique/), also known as VLANs. There are 2 types of zones: Open or Restricted.
An Open zone allows devices to connect freely to a network resource in that zone. If the zone is of type Restricted, the devices that connect to a network resource of this zone must be authorized by the administrator.

- Each network appliance expose the zones to their clients using Wi-Fi access points and/or wired ethernet ports.
- Client devices connected in same zone can communicate with each other, no matter which physical network appliance they are connected to.

![Create account](./account-overview.jpg)


To manage your networks, see the example of possible [operational implementation](/docs/presentation/operational-implementation).

Manage your Network in two ways:

- With [Dashboard SentinelC](/docs/user-guides/dashboard/dashboard-overview).
Designed for small networks. [The administrator](https://www.sentinelc.com/faq/support-administratif/quelle-est-la-difference-entre-un-administrateur-privilegie-et-un-administrateur-dun-ou-plusieurs-comptes/) is not a system operator. Each administrator has an overview of his network. He can create, view his Wi-Fi networks and administer them. He can also see the devices that connect to the network and block them if necessary.
The administrator has an access to the activity of his network and he can also manage the user access.


- With [Admin portal](/docs/user-guides/admin-portal/access). 
Designed for large networks. [The administrator](https://www.sentinelc.com/faq/support-administratif/quelle-est-la-difference-entre-un-administrateur-privilegie-et-un-administrateur-dun-ou-plusieurs-comptes/) is a system operator with access to advanced functions. Each administrator has a detailed view of all his networks. It can manage accounts, locations, and appliance inventory.
It can also manage devices and connections on the network, as well as manage administrator access. It can also manage incidents on the network and perform monitoring through several actions: packets capture, Wi-Fi scans, rogue Wi-Fi or port scans.
