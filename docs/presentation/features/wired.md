---
title: Wired

---

Depending on the number of ports the machine has, The SentinelC application offers:

- Ports can be associated to a restricted or open.
- Visibility of ports in the system.
- Quality of Service.
- Possibility to block a port.
- Scanning of ports in the system.