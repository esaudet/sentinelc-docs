---
title: Multi-tenancy

---

A platform operator can create administrators who just have access to a subset of the accounts:
- Ability to edit or delete accesses that have been added to a user.
- Privileged permission:
    -  The administrator can do everything and see on all accounts and their dependencies
- Local permission:
    - An administrator (authorized access to 1 or more accounts), has his perimeter limited to his own access. This means that this administrator can do all the actions/visualizations on the accounts to which he has access.