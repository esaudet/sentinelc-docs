---
title: Zones

---

A zone constituting a logical grouping which makes it possible to segment a local network into a virtual subnet. It allows grouping together a set of devices requiring access to specific network resources in addition to providing additional security by allowing a higher degree of control over which devices have access to each other.
There are 3 types of zones: Private, Public, Portal.

- The user can only create zones of type Private.
- The Public and Portal zone types are automatically managed by the system.

For example, the Public zone groups the devices which have access only to the Internet and which can communicate with each other. A private zone provides internet access in addition to supporting a local area network between all connected devices that are able to communicate with each other.
The Portal zone, for its part, offers limited Internet access to devices that connect to it via the Portal Wi-Fi network while remaining isolated from each other.
There are 2 access policies. These access policies can only be assigned to Private type zones.

- Opened
- Restraint

A so-called open zone allows devices to connect freely to a network resource in this zone. If the zone is of the Restricted type, devices that connect to a network resource in this zone must be authorized by the administrator. Note that for the moment, it is not possible to configure a restricted zone from the application. 


![diagram-zones](./diagram-zones.png)

Zones are a powerful building block for configuring advanced use-cases:
- Use one or more `iot` zones to isolate less trusted devices from the rest of your network.
- Combine the [QoS](./qos) feature with a `cameras` zone to guarantee your video monitoring solution always have enough bandwidth available.
- Use a public zone with limited internet access for your guest network.
- Seperate business from personal usage of your remote workers.
- Link zones from different network locations together using the built-in [VPN](./vpn).