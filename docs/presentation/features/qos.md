---
title: Quality of Service (QoS)

---

QoS allows sharing of the available WAN bandwidth.

High-level operation:

- The bandwidth available on the WAN connection must be known and reliable.
- ** QoS will not work on a shared internet connection **.
  - The sentinelc router must be plugged directly into the modem.
  - All the function depends on the fact that we control the buffer of incoming and outgoing packets from the internet.
  - If the buffer is out of control, the results will be completely crazy. ** Don't even try. **
  - Internet speed must be reliable. If the speed is unreliable, the maximum flow must be artificially lowered to a reliable speed.
- A limit of about 90% of the available bandwidth must be applied on the WAN connection. This ensures that the SentinelC router is in control of all internet access buffers.
- Without any other configuration:
  - Each zone will receive its percentage of bandwidth. Example, if there are 2 zones, 50% bandwidth guaranteed.
  - Each active IP address within each zone will receive its equal share of outgoing ** bandwidth **.
    - Example, if there are 10 devices, each will be limited to 10% upload.
    - We cannot guarantee the sharing in download, but as the upload is shared fairly, normally, the download is also balanced.
- If there is bandwidth available, you can always exceed your allocated quota. Example if there are 10 devices connected in an area with 50% of the bandwidth, if everyone transfers at the same time, each device will have 10% of 50% or 5% of the total internet bandwidth. On the other hand, if only one device is active, it can easily have 100% of the bandwidth.
- The bandwidth is always distributed evenly in an area. Currently we cannot prioritize one device more than another within the same area.
- Limitation on one zone:
  - We can set a "course", ie a maximum impossible to exceed in a zone.
    - Even if there is bandwidth available, a zone with a configured maximum bandwidth will not be able to exceed the limit.
  - You can put a guaranteed reserve on an area.
    - We guarantee that the configured bandwidth will ALWAYS be available as a priority for the zone.
    - Ideal for reserving bandwidth for the proper functioning of a service. Example, stream from IP cameras.

### Note about sharing between users of the same zone

#### Upload

- In upload, during QoS processing, we have access to all the information of the outgoing packets.
- We can therefore classify without problem according to:
  - The area
  - IP address (user)

#### Download

- QoS-related processing for download packets takes place * before * NAT.
- We therefore do not know for which zone or for which internal IP (user) the packet is intended.
- The solution used is the `act_connmark` module as well as `connection mark` applied to the stream.
- This allows to mark a flow with a number (only one).
- We use the ZONE number (vlan id) as a mark.
- When the packet comes back, we can therefore apply the QoS according to the destination zone.
- On the other hand, we have no identification for the individual user (IP).
- We cannot therefore guarantee that the download is equitably shared between 2 users of the same zone.